#pragma once

/// <summary>
/// This is brief defination of class
/// </summary>
/// 
/// 
/// This is detailed description of class
class myclass1
{
public:

	///Brief description
	/// 
	/// This is the more detailed version.More details
	/// @param a This defines parameter a 
	/// @param b This defines parameter b
	/// @return no return.
	/// @see anotherMemberFunction() anotherMemberFunction2()
	void memberFunction(int a, int b);


	void anotherMemberFunction();
	void anotherMemberFunction2();

	void memberFunction();

};

